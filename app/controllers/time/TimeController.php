<?php

namespace app\controllers\time;
use vendor\core\DB;

class TimeController{


    public function getTime(){
            header('Time:' . date('Y-m-d H-i-s'));
    }

    public function changeTimezone(){

        $timeZone = json_decode(file_get_contents('php://input'));
        date_default_timezone_set($timeZone->timezone);
        header('Time:' . date('Y-m-d H-i-s'));
    }

    public function insertZone(){
     
         $zones = [
          'Europe/Amsterdam',
          'Europe/Andorra',   
          'Europe/Astrakhan',   
          'Europe/Athens',
          'Europe/Belgrade',   
          'Europe/Berlin',   
          'Europe/Bratislava',   
          'Europe/Brussels',
          'Europe/Bucharest',   
          'Europe/Budapest',   
          'Europe/Busingen',   
          'Europe/Chisinau',
          'Europe/Copenhagen',   
          'Europe/Dublin',   
          'Europe/Gibraltar',   
          'Europe/Guernsey',
          'Europe/Helsinki',   
          'Europe/Isle_of_Man',   
          'Europe/Istanbul',   
          'Europe/Jersey',
          'Europe/Kaliningrad',   
          'Europe/Kiev',   
          'Europe/Kirov',   
          'Europe/Lisbon',
          'Europe/Ljubljana',   
          'Europe/London',   
          'Europe/Luxembourg',   
          'Europe/Madrid',
          'Europe/Malta',   
          'Europe/Mariehamn',   
          'Europe/Minsk',   
          'Europe/Monaco',
          'Europe/Moscow',   
          'Europe/Oslo',   
          'Europe/Paris',   
          'Europe/Podgorica',
          'Europe/Prague',   
          'Europe/Riga',   
          'Europe/Rome',   
          'Europe/Samara',
          'Europe/San_Marino',   
          'Europe/Sarajevo',   
          'Europe/Saratov',   
          'Europe/Simferopol',
          'Europe/Skopje',   
          'Europe/Sofia',   
          'Europe/Stockholm',   
          'Europe/Tallinn',
          'Europe/Tirane',   
          'Europe/Ulyanovsk',   
          'Europe/Uzhgorod',   
          'Europe/Vaduz',
          'Europe/Vatican',   
          'Europe/Vienna',   
          'Europe/Vilnius',   
          'Europe/Volgograd',
          'Europe/Warsaw',   
          'Europe/Zagreb',   
          'Europe/Zaporozhye',   
          'Europe/Zurich'
          ];
        foreach ($zones as $zone){
            DB::connect("INSERT INTO `zones` (`zone`) VALUES ('$zone')");
        }

    }

}
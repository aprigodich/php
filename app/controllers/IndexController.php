<?php

namespace app\controllers;
use app\models\File;
use app\models\Message;
use app\models\Zone;
use vendor\core\DB;
use vendor\core\View;

class IndexController{

    public function index(){
        $messagesIsNotRead = [];
        if(\Auth::user()){
            $messagesIsNotRead = Message::
            where('to_who_sended', '=', \Auth::user()->username)
                ->where('is_read', '=', 0)
                ->get()
                ->all();
        }
            View::make('files/index',
                [
                    'messagesIsNotRead' => count($messagesIsNotRead),
                    'query' => Zone::get()->all()
                ]);
    }



}
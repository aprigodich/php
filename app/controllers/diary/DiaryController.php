<?php

namespace app\controllers\diary;

use app\models\Diary;
use vendor\core\View;

class DiaryController{

    /**
     * @var string
     */
    public $dirName = '';

    /**
     * @return View
     */
    public function index(){

        $previewFiles = scandir('images/diary/preview/');
        foreach ($previewFiles as $previewFile){

            if($previewFile !== '.' && $previewFile !== '..'){
                unlink('images/diary/preview/' . $previewFile);
            }
        }

        if(\Auth::check() && \Auth::user()->username === 'admin'){
            $diary = array_chunk(Diary::get()->all(), 2);
        }else{
            $diary = array_chunk(Diary::where('is_publish', '=', 1)->get()->all(), 2);
        }
        return View::make('diary/index',
            [
                'diary' => $diary
            ]);
    }

    /**
     *
     */
    public function create(){

        if($url = $this->saveFile('release')){
            Diary::create([
                'email' => $_REQUEST['email'],
                'username' => $_REQUEST['username'],
                'text' => $_REQUEST['text'],
                'url' => $url,
            ]);
        }
        redirect('/diary');
    }

    /**
     * @param $id
     */
    public function delete($id){

        Diary::where('id', '=', $id)->delete();
        return redirect('/diary');

    }

    /**
     * @param $id
     * @return View
     */

    public function edit($id){

        return View::make('diary/edit',
            [
                'diary' => Diary::where('id', '=', $id)->get()->first()
            ]);

    }

    /**
     * @param $id
     */
    public function update($id){
        Diary::where('id', '=', $id)->update(
            [
                'username' => $_REQUEST['username'],
                'email' =>  $_REQUEST['email'],
                'text' =>  $_REQUEST['text'],
                'is_publish' =>  isset($_REQUEST['is_publish']) ? 1 : 0,
            ]
        );
        return redirect('/diary');
    }

    /**
     *
     */
    public function preview(){

        $this->saveFile('preview');
    }

    /**
     * @param $dirToSaveFile
     * @return string
     * @throws \Error
     */
    public function saveFile($dirToSaveFile){

        if($_FILES){

            $dir = $_FILES['diaryImage']['tmp_name'];

            $file = getimagesize($dir);

            $dirName = "/images/diary/$dirToSaveFile/" .  date('Y-m-d H-m-s') . '.' . $this->checkFile($_FILES['diaryImage']['name']);

        }else{
            throw new \Error('Choice file');
        }

        if($file[0] > 320 || $file[1] > 240){
            $resizeImage = imagecreatetruecolor(320, 240);
            switch ($this->checkFile($_FILES['diaryImage']['name'])){
                case 'png' :
                    $source = imagecreatefrompng($dir);
                    $image = 'imagepng';
                    break;
                case 'jpeg' :
                    $source = imagecreatefromjpeg($dir);
                    $image = 'imagejpeg';
                    break;
                case 'gif' :
                    $source = imagecreatefromgif($dir);
                    $image = 'imagegif';
                    break;
            }

            imagecopyresized($resizeImage, $source, 0, 0, 0, 0, 320, 240, getimagesize($dir)[0], getimagesize($dir)[1]);

            if($image($resizeImage, ROOT . 'public' . $dirName)){
                echo $dirName;
                $this->dirName = $dirName;
                imagedestroy($resizeImage);
                imagedestroy($source);
                return $dirName;
            }else{
                throw new \Error('File Not Save');
            }

        }else{
            if(move_uploaded_file($_FILES['diaryImage']['tmp_name'], ROOT . 'public' . $dirName)){
                echo $dirName;
                return $dirName;
            }
        }
    }

    /**
     * @param $file
     * @return string
     */
    public function checkFile($file){
        $info = new \SplFileInfo($file);
        return $info->getExtension();
    }
}
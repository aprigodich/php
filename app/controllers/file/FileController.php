<?php
namespace app\controllers\file;

use app\models\File;
use vendor\core\View;

class FileController{

    public function upload(){

        if($_FILES && !$_FILES['file']['error'] && $_FILES['file']['size'] < 555000){
            $uploadDir = ROOT . 'storage/' . $_FILES['file']['name'];
            $typeFile = $this->checkFile($_FILES['file']['name']);
            if($typeFile === 'png' || $typeFile === 'jpeg'){
//                $size = $_FILES['file']['size'];
                move_uploaded_file( $_FILES['file']['tmp_name'], $uploadDir);
                File::create([
                    'size' => $_FILES['file']['size'],
                    'url' => $uploadDir
                ]);
//                DB::connect("INSERT INTO `files` (`size`, `url`) VALUES('$size', '$uploadDir')");
                echo 'File is SAVE';
            }else{
                echo 'FILE is NOT SAVE';
            }
        }else{
            redirect('/');
        }
    }

    public function getFile($id){
        View::make('files/information',
            [
                'query' => File::where('id', '=', $id)->get()->first()
            ]);

    }

    public function checkFile($file){
        $info = new \SplFileInfo($file);
        return $info->getExtension();
    }

}

<?php

namespace app\controllers\message;

use app\models\Message;
use vendor\core\DB;
use vendor\core\View;

class MessageController{

    public function index(){

        $toWhoSenden = \Auth::user()->username;

        if(isset(getallheaders()['Content-Type']) && getallheaders()['Content-Type'] === 'text/plain'){
//            $messagesNotRead = DB::connect("SELECT * FROM `messages` WHERE `to_who_sended` = '$toWhoSenden' AND `is_read` = '0' ", true);
            $messagesNotRead = Message::where('to_who_sended', '=', \Auth::user()->username)
                                        ->where('is_read', '=', 0)
                                        ->get()
                                        ->all();
            if($messagesNotRead){
                DB::connect("UPDATE  `messages` SET `is_read` = '1' WHERE `to_who_sended` = '$toWhoSenden'");
                echo json_encode($messagesNotRead);
            }
            return $messagesNotRead;

        }

            $messages = DB::connect("SELECT * FROM `messages` WHERE `to_who_sended` = '$toWhoSenden'", true, true);
            if(!empty($messages)){
                foreach ($messages as $message){
                    if($message['is_read'] === '0'){
                        $id = $message['id'];
                        DB::connect("UPDATE  `messages` SET `is_read` = '1' WHERE `id`= '$id'");
                    }
                }
            }

        if(\Auth::check()){


            return View::make('messenger/index', [
                'messagesIsNotRead' => 0,
                'messages' => $messages
            ]);
        }
        else{
            abort(404, 'Permission');
        }

    }


    public function create(){
        $from_who_sended = \Auth::user()['username'];
        if(\Auth::check()) {
            if (isset(getallheaders()['Content-Type']) && getallheaders()['Content-Type'] === 'text/plain') {
                $query = $this->getContent();
                $toWhoSended = $query->user;
                DB::connect("INSERT INTO `messages` (`message`, `to_who_sended`, `from_who_sended`) VALUES ('$query->message', '$toWhoSended', '$from_who_sended')");
            }
        }else{
            abort(404, 'Permission');
        }
    }


    public function getContent(){
        return (json_decode(file_get_contents('php://input')));
    }

}
<?php

namespace app\controllers\auth;


use vendor\core\View;

class AuthController {

    static $redirect = '';

    public function getRegister(){

        View::make('auth/register');
    }

    public function postRegister(){
        \Auth::register($_REQUEST['username'], $_REQUEST['password'], $_REQUEST['confirmPassword']);
    }

    public function logout(){
        \Auth::logout();
    }

    public function login(){
        \Auth::login($_REQUEST['username'], $_REQUEST['password']);
    }

}
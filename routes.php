<?php

Router::get('/', ['as' => 'upload', 'uses' => 'app\controllers\IndexController@index']);

// AUTH
Router::get('/auth/register', ['as' => 'get.register', 'uses' => 'app\controllers\auth\AuthController@getRegister']);
Router::post('/auth/register', ['as' => 'post.register', 'uses' => 'app\controllers\auth\AuthController@postRegister']);
Router::post('/auth/logout', ['as' => 'post.register', 'uses' => 'app\controllers\auth\AuthController@logout']);
Router::post('/auth/login', ['as' => 'post.register', 'uses' => 'app\controllers\auth\AuthController@login']);

//USERS
Router::post('/get/user', ['as' => 'get.user', 'uses' => 'app\controllers\UserController@getUser']);

//INDEX
Router::get('/upload', ['as' => 'file.upload', 'uses' => 'app\controllers\IndexController@upload']);
Router::post('/get/file/{id}', ['as' => 'file.get', 'uses' => 'app\controllers\file\FileController@getFile'])->with(['id' => '([0-9]+)']);

//TIME
Router::get('/get/time', ['as' => 'time.get', 'uses' => 'app\controllers\time\TimeController@getTime']);
Router::get('/get/users', ['as' => 'time.get', 'uses' => 'app\controllers\time\TimeController@getTime']);
Router::get('/insert', ['as' => 'file.insert', 'uses' => 'app\controllers\time\TimeController@insertZone']);
Router::get('/change/timezone', ['as' => 'time.change', 'uses' => 'app\controllers\time\TimeController@changeTimezone']);

//Message
Router::get('/message', ['as' => 'message.index', 'uses' => 'app\controllers\message\MessageController@index']);
Router::post('/message/create', ['as' => 'message.create', 'uses' => 'app\controllers\message\MessageController@create']);

Router::get('/webgl', ['as' => 'webgl', 'uses' => 'app\controllers\webgl\WebGlController@index']);
Router::get('/webgl2', ['as' => 'webgl2', 'uses' => 'app\controllers\webgl\WebGlController@index2']);
Router::get('/webgl3', ['as' => 'webgl2', 'uses' => 'app\controllers\webgl\WebGlController@index2']);

//Diary
Router::get('/diary', ['as' => 'diary.index', 'uses' => 'app\controllers\diary\DiaryController@index']);
Router::post('/diary/preview', ['as' => 'diary.preview', 'uses' => 'app\controllers\diary\DiaryController@preview']);
Router::post('/diary/create', ['as' => 'diary.create', 'uses' => 'app\controllers\diary\DiaryController@create']);
Router::post('/diary/delete/{id}', ['as' => 'diary.create', 'uses' => 'app\controllers\diary\DiaryController@delete'])->with(['id' => '([0-9]+)']);
Router::get('/diary/edit/{id}', ['as' => 'diary.edit', 'uses' => 'app\controllers\diary\DiaryController@edit'])->with(['id' => '([0-9]+)']);
Router::POST('/diary/update/{id}', ['as' => 'diary.update', 'uses' => 'app\controllers\diary\DiaryController@update'])->with(['id' => '([0-9]+)']);

//Router::group('/files' ,
//);

@require('layout/main')
@section('content')
<div class="container" style="width: 30%">

    <form class="form-signin" method="post" action="/auth/register">
        <h2 class="form-signin-heading">Register</h2>
        <label for="inputEmail" class="sr-only">User Name</label>
        <input type="text" name="username" class="form-control" placeholder="username" required="" autofocus="">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password" required="">
        <label for="inputPassword" class="sr-only">Password Confirm</label>
        <input type="password" name="confirmPassword" class="form-control" placeholder="Password" required="">
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

</div>

@end
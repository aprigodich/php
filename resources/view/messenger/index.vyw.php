@require('layout/main')
@section('content')

<script src="/js/app/controllers/message/UserController.js"></script>
<script  src="/js/app/controllers/message/MessageController.js" defer></script>
<script src="/js/vendor/core/ajax/Ajax.js"></script>

<div id = "messages">
    <?php foreach ($messages as $message):?>
    <div class="col-lg-12">
        <h2><?php echo 'Message From : ' . $message['from_who_sended']?></h2>
        <p><?php echo 'Created : ' . $message['created_at']?></p>
        <p><?php echo 'Message : ' . $message['message'] ?></p>
    </div>
    <?php endforeach; ?>
</div>

<div class="jumbotron" onclick="MessageController.hideMultiple()">
    <div class="container">
        <h1>Send Message</h1>
        <div class="form-group" style="max-width:20%;">
            <label for="user">Enter User Name:</label>
            <input type="text" class="form-control" id="user" onkeyup="UserController.getUser()">
            <select multiple hidden id="select" style="width:230px;" onclick="MessageController.getUserValue(event)">
            </select>
        </div>

        <div class="form-group">
            <label for="message">Message:</label>
            <textarea class="form-control" rows="5" id="message" style="max-width:100%;"></textarea>
        </div>
        <button type="submit" name="send" onclick="MessageController.sendMessage()" class="btn btn-success">Send</button>
        <audio src="/mp3/sms.mp3" id="audio"  ></audio>
    </div>

</div>


@end
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="/js/vendor/core/gl-matrix/dist/gl-matrix.js"></script>
    <script>



//        window.onload = function () {
//            window.WebGl = new WebGl();
//            window.WebGl.init();
//        };
//
//
//        function WebGl(){
//
//
//           this.canvas = document.getElementById('canvas');
//
//            //installation webGl into browser
//           this.init = function () {
//
//
//                var names = ['webgl', 'webkit-3d', 'moz-webgl', 'experimental-webgl'];
//
//                for (var i = 0; i < names.length; i++){
//                    try {
//                        this.gl = this.canvas.getContext(names[i]);
//                    }catch (e){}
//                    if(this.gl) break;
//                }
//            };
//
//
//            //создание квадрата, в массиве параметры цвета по умолчанию черный
//            this.callCanvas = function (resizeWidth, resizeHeight, colors = [0.0,0.0,0.0,1.0]) {
//                this.resizeAndPositionCanvas(resizeWidth, resizeHeight);
//                //params color
//                this.gl.clearColor(colors[0],colors[1],colors[2],colors[3]);
//
//                this.cclear();
//            };
//
//
//            //resize
//            this.resizeAndPositionCanvas = function (resizeWidth, resizeHeight) {
//                this.canvas.width = resizeWidth;
//                this.canvas.height = resizeHeight;
//            };
//
//            this.cclear = function () {
//
//                // очистить буфер цвета и буфер глубины
//                this.gl.clear(this.gl.DEPTH_BUFFER_BIT | this.gl.COLOR_BUFFER_BIT);
//
////                this.gl.viewport(0, 0, this.canvas.width, this.canvas.height);
//            }
//        }


//window.onload = function () {
//            window.WebGl = new WebGl();
//            window.WebGl.init();
//        };
//
//
//        function WebGl() {
//            this.webGLStart = function() {
//
//                this.canvas = document.getElementById('canvas');
//                this.init();
////                initShaders();
//                this.initBuffers();
//
//                this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
//                this.gl.enable(this.gl.DEPTH_TEST);
//
//                this.drawScene();
//            };
//
//
//            //installation webGl into browser
//            this.init = function () {
//
//
//                var names = ['webgl', 'webkit-3d', 'moz-webgl', 'experimental-webgl'];
//
//                for (var i = 0; i < names.length; i++) {
//                    try {
//                        this.gl = this.canvas.getContext(names[i]);
//                    } catch (e) {
//                    }
//                    if (this.gl) break;
//                }
//            };
//
//            this.initBuffers = function() {
//                this.triangleVertexPositionBuffer = this.gl.createBuffer();
//
//
//
//            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.triangleVertexPositionBuffer);
//
//            var vertices = [
//                0.0, 1.0, 0.0,
//                -1.0, -1.0, 0.0,
//                1.0, -1.0, 0.0
//            ];
//
//            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(vertices), this.gl.STATIC_DRAW);
//
//               this.triangleVertexPositionBuffer.itemSize = 3;
//               this.triangleVertexPositionBuffer.numItems = 3;
//            };
//
//
//
//            this.drawScene = function() {
//                this.gl.viewport(0, 0, this.gl.viewportWidth, this.gl.viewportHeight);
//                this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
//                mat4.perspective(45, this.gl.viewportWidth / this.gl.viewportHeight, 0.1, 100.0, pMatrix);
//            };
//        }


var gl;
var shaderProgram;
var vertexBuffer;
var indexBuffer;
var mvMatrix = mat4.create();
var pMatrix = mat4.create();
var angle = 2.0;//  угол поворота в радианах
var zTranslation = -2.0; // смещение по оси Z
var texture;
var colorBuffer;
// установка шейдеров
function initShaders() {
    // получаем шейдеры
    var fragmentShader = getShader(gl.FRAGMENT_SHADER, 'shader-fs');
    var vertexShader = getShader(gl.VERTEX_SHADER, 'shader-vs');
    //создаем объект программы шейдеров
    shaderProgram = gl.createProgram();
    // прикрепляем к ней шейдеры
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    // связываем программу с контекстом webgl
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Не удалсь установить шейдеры");
    }

    gl.useProgram(shaderProgram);
    // установка атрибута программы
    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    // делаем доступным атрибут для использования
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
//    shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "aVertexColor");
//    gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);


    // создания переменных uniform для передачи матриц в шейдер
//    shaderProgram.MVMatrix = gl.getUniformLocation(shaderProgram, "uMVMatrix");
//    shaderProgram.ProjMatrix = gl.getUniformLocation(shaderProgram, "uPMatrix");
}

function setMatrixUniforms(){
    gl.uniformMatrix4fv(shaderProgram.ProjMatrix,false, pMatrix);
    gl.uniformMatrix4fv(shaderProgram.MVMatrix, false, mvMatrix);
}


// Функция создания шейдера по типу и id источника в структуре DOM
function getShader(type,id) {
    var source = document.getElementById(id).innerHTML;
    // создаем шейдер по типу
    var shader = gl.createShader(type);
    // установка источника шейдера
    gl.shaderSource(shader, source);
    // компилируем шейдер
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert("Ошибка компиляции шейдера: " + gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }
    return shader;
}
// установка буфера вершин
function initBuffers() {
    // установка буфера вершин
    // массив координат вершин объекта
    var vertices = [
        // лицевая часть
        -0.5, -0.5, 0.5,
        -0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,
        0.5, -0.5, 0.5
    ];

    var indices = [0, 1, 2, 2, 3, 0];

    vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    vertexBuffer.itemSize = 3;


//    var colors = [
//        1.0, 0.0, 0.0,
//        0.0, 0.0, 1.0,
//        0.0, 1.0, 0.0
//    ];
//
//    colorBuffer = gl.createBuffer();
//    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
//    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);


    // указываем кол-во точек
    indexBuffer.numberOfItems = indices.length;

}
// отрисовка
function draw() {
    // установка области отрисовки
//    gl.clearColor(0.0, 0.0, 0.0, 1.0);
//    gl.viewport(0, 0, 500, 500);
//
//    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    // указываем, что каждая вершина имеет по три координаты (x, y, z)
//    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
//    gl.vertexAttribPointer(shaderProgram.vertexColorAttribute,
//        vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute,
        vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);
    // отрисовка примитивов - треугольников
//    gl.drawArrays(gl.TRIANGLES, 0, vertexBuffer.numberOfItems);
    gl.drawElements(gl.TRIANGLES, indexBuffer.numberOfItems, gl.UNSIGNED_SHORT,0);
}

function setupWebGL()
{
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT  || gl.DEPTH_BUFFER_BIT);
    gl.viewport(0, 0, 500, 500);

//    angle += 0.01;
//    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
//    mat4.perspective(pMatrix, 1.04, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0);
//    mat4.identity(mvMatrix);
//    mat4.translate(mvMatrix,mvMatrix,[0, 0, zTranslation]);
//    mat4.rotate(mvMatrix,mvMatrix, angle, [0, 1, 0]);
}

function handleTextureLoaded(image, texture) {

    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
//    steupTextureFilteringAndMips(image.width, image.height);
//    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
//    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
}

function isPowerOf2(value) {
    return (value & (value - 1)) == 0;
};

function steupTextureFilteringAndMips(width, height) {
    if (isPowerOf2(width) && isPowerOf2(height)) {
        // the dimensions are power of 2 so generate mips and turn on
        // tri-linear filtering.
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    }
    else {
        // at least one of the dimensions is not a power of 2 so set the filtering
        // so WebGL will render it.
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    }
}




function setTextures() {
    texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    var image = new Image();
    image.src = "/images/texture/download.jpg";
    image.onload = function () {
        handleTextureLoaded(image, texture);

        setupWebGL();
        draw();
    };


    shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");
    gl.uniform1i(shaderProgram.samplerUniform, 0.5);
}



window.onload=function(){
    // получаем элемент canvas
    var canvas = document.getElementById("canvas");
    try {
        // Сначала пытаемся получить стандартный контекст WegGL
        // Если не получится, обращаемся к экспериментальному контексту
        gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    }
    catch(e) {}

    // Если контекст не удалось получить, выводим сообщение
    if (!gl) {
        alert("Ваш браузер не поддерживает WebGL");
    }
    if(gl){
        // установка размеров области рисования
        gl.viewportWidth = canvas.width;
        gl.viewportHeight = canvas.height;
//        document.addEventListener('keydown', handleKeyDown, false);
        // установка шейдеров
        initShaders();
        // установка буфера вершин
        initBuffers();
        setTextures();
//        setupWebGL();
//        setMatrixUniforms();
        // покрасим фон в бледно-розовый цвет
        // отрисовка сцены
//        draw();
//        (function animloop(){
//            setupWebGL();
//            setMatrixUniforms();
//            draw();
//            requestAnimFrame(animloop);
//        })();
    }
};

function handleKeyDown(e){
    switch(e.keyCode)
    {
        // изменяем угол поворота
        case 39:  // стрелка вправо
            angle+=0.1;
            break;
        case 37:  // стрелка влево
            angle-=0.1;
            break;
        // изменяем смещение по оси Z
        case 40:  // стрелка вниз
            zTranslation+=0.1;
            break;
        case 38:  // стрелка вверх
            zTranslation-=0.1;
            break;
    }
}

//window.requestAnimFrame = (function(){
//return  window.requestAnimationFrame       ||
//        window.webkitRequestAnimationFrame ||
//        window.mozRequestAnimationFrame    ||
//        window.oRequestAnimationFrame      ||
//        window.msRequestAnimationFrame     ||
//        function(callback) {
//            return window.setTimeout(callback, 1000/60);
//        };
//})();



    </script>
    <title>Title</title>
</head>
<body>


<canvas id="canvas" width="500" height="500" ></canvas>
<script id="shader-fs" type="x-shader/x-fragment">
precision highp float;
uniform sampler2D uSampler;
varying vec2 vTextureCoords;

  void main(void) {
    gl_FragColor = texture2D(uSampler, vTextureCoords);
  }
</script>

<script id="shader-vs" type="x-shader/x-vertex">
 attribute vec3 aVertexPosition;
 varying vec2 vTextureCoords;

  void main(void) {
    gl_Position = vec4(aVertexPosition, 1.0);
    vTextureCoords = vec2(aVertexPosition.x+0.5,aVertexPosition.y+0.5);
  }
</script>
<!--<button onclick="window.WebGl.callCanvas(400, 400, [1.0,1.0,0.0,1.0])">but</button>-->
<button onclick="window.WebGl.webGLStart()">but</button>

</body>
</html>
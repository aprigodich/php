
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Messenger</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php if (!Auth::check()):?>
            <div class="navbar-form navbar-right">
                <form method="POST" action="/auth/login">
                    <div class="form-group">
                        <input type="text" placeholder="Username" name="username" required="" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" name="password" required="" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                    <a href="/auth/register" class="btn btn-success">Register</a>
                    <a href="/diary" class="btn btn-success">Diary</a>
                </form>
             </div>
            <?php endif; ?>
            <?php if (Auth::check()):?>
                <form method="post" action="/auth/logout" class="navbar-form navbar-right">
                    <button type="submit" class="btn btn-success">Logout</button>
                </form>
                <form  action="/message" class="navbar-form navbar-right">

                    <button type="submit" name="send" value="1" class="btn btn-success">
                        <span class="glyphicon glyphicon-envelope" aria-hidden="true">
                        </span>
<!--                        <span class="glyphicon-class">--><?php //echo $messagesIsNotRead ?><!--</span>-->
                    </button>
                    <a href="/diary" class="btn btn-success">Diary</a>
                </form>
            <?php endif; ?>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
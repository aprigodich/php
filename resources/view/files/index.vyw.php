@require('layout/main')

@section('content')

    <h1 id="time"></h1><br>
    <label>Тайм зоны</label>
    <form enctype="multipart/form-data" action="/change/timezone" method="get">
    <select id="timeZone" onchange="changeTimeZone(); getTime();">
        <option>Выберите тайм зону</option>
        <?php foreach ($query as $zone):?>
        <option value="<?php echo $zone->zone?>"><?php echo $zone->zone?></option>
        <?php endforeach;?>
    </select>
    </form>
    <br><br><br><br>

<!--    <h1>Загрузить файл</h1>-->
<!--    <form enctype="multipart/form-data" action="/upload" method="POST">-->
<!--        <input type="file" name="file">-->
<!--        <button>Сохранить</button>-->
<!--    </form><br>-->
<!--        <h1>Информация об файле</h1>-->
<!--        <br>-->
<!--    <form id="getFile" enctype="multipart/form-data" action="/get/file" method="POST">-->
<!--        <label>Введите ID</label>-->
<!--        <input id="file" name="id" value="">-->
<!--        <button onclick="setFile(event)">Проверить</button>-->
<!--    </form>-->

<script>

    function setFile() {
        event.preventDefault();
        document.getElementById('getFile').action = '/get/file/' +  document.getElementById('file').value;
        document.getElementById('getFile').submit();
    }

    window.onload = function () {
        getTime();
       this.interval = setInterval(getTime, 1000);
    };



    function getTime() {
        var req = new XMLHttpRequest();
        req.open('GET', 'get/time', false);
        req.setRequestHeader('Content-Type', 'application/json');
        req.send();
        document.getElementById('time').innerHTML = req.getResponseHeader('time');
    }

    function changeTimeZone() {
        clearInterval(this.interval);
        var selected = document.getElementById('timeZone').options[document.getElementById('timeZone').selectedIndex].value;
        var req = new XMLHttpRequest();
        req.open('POST', 'change/timezone', false);
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify({timezone: selected}));
        if(req.getResponseHeader('time')){
                document.getElementById('time').innerHTML = req.getResponseHeader('time');
            this.interval = setInterval(changeTimeZone, 1000);
        }
    }

</script>

@end
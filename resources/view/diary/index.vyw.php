@require('layout/main')

@section('content')
<script src="/js/app/controllers/diary/DiaryController.js" defer></script>
<script src="/js/vendor/core/ajax/Ajax.js"></script>
﻿
<!--<img src="/images/diary/Screenshot from 2016-12-22 15-22-57.png">-->
<div class="container">
    <div class="row" id="diary">
        <?php foreach ($diary as $items): ?>
            <?php foreach ($items as $item): ?>
            <div class="col-md-6">
                <h3>Задача <?php echo $item->text ?></h3>
                <h3>Пользователь <?php echo $item->username ?></h3>
                <h3>Мэил <?php echo $item->email ?></h3>
                <img src="<?php echo $item->url ?>">
                <?php if(Auth::check() && Auth::user()->username === 'admin'): ?>
                    <h3><?php echo $item->is_publish ? 'Опубликовано' : 'Не опубликовано' ?></h3><br>
                <?php endif;?>
                <?php if(Auth::check() && Auth::user()->username === 'admin'): ?>
                    <form  action="<?php echo '/diary/delete/' . $item->id?>" method="POST">
                        <button  type="submit" class="btn btn-success">удалить</button>
                    </form><br>
                    <form  action="<?php echo '/diary/edit/' . $item->id?>" method="GET">
                        <button  type="submit" class="btn btn-success">Редактировать</button>
                    </form><br>
                <?php endif; ?>
                <hr>
            </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
    <hr>
    <div class="jumbotron">
        <h3>Создать задачу</h3><br>
        <form enctype="multipart/form-data" action="/diary/create" method="POST">
            <label for="basic-url">username</label>
            <input class="form-control" id="diaryUsername" type="text"  name="username" style="width: 30%" required>
            <label for="basic-url">email</label>
            <input class="form-control" id="diaryEmail" type="email"  name="email" style="width: 30%" required><br>
            <label for="basic-url">text</label><br>
            <textarea rows="5"  cols="45" id="diaryText" name="text" ></textarea><br><br>
            <input type="file" id="diaryImage" name="diaryImage"><br>
            <button  type="submit" class="btn btn-success">Сохранить</button>
            <a class="btn btn-success" onclick="DiaryController.diaryCratePreview()">Предосмотр</a>
        </form><br>
    </div>

    <hr>


</div>

@end
<?php


class Router{

    public static $routes = [] ;

    public static $queryArray = [];

    /**
     * @param $url
     * @param $method
     * @return bool
     * @throws ErrorException
     */
    public static function run($url, $method){
        foreach (self::$routes as $route){
                if(preg_match('~^' . self::matchingGetPostMethods($route['url'], $route['method']) . '$~', $url, $params)) {
                    if($route['method'] === $method){
                    array_shift($params);

                    if($params){
                        self::uses($route, $params);
                    }else{
                        self::uses($route);
                    }

                    return true;
                }else{
                        throw new ErrorException('Method Not Allowed');
                    }
            }
        }
    }

    /**
     * @param $url
     * @param $method
     * @return string
     * @throws ErrorException
     */
    public static function matchingGetPostMethods($url, $method){
        $pregMatch = '';

        $getPregMatch = $url . '(\?[?=\w\=&]*)?';

        $postPregMatch = $url;

        if($method === 'GET'){

            $pregMatch = $getPregMatch;

        }

        if($method === 'POST'){

            $pregMatch= $postPregMatch;

        }
        if(!$method === 'GET' && !$method === 'POST'){


            throw new ErrorException('Method Not Allowed');
        }

        return $pregMatch;

    }

    /**
     * @param $route
     * @param null $params
     */
    public static function uses($route, $params = null){
            $routeExplodeUses = explode('@', $route['uses']);

            $controller = new $routeExplodeUses[0];

            $method = $routeExplodeUses[1];

            if($params){
                call_user_func_array([$controller, $method], $params);
            }else{
                call_user_func([$controller, $method]);
            }
    }

    /**
     * @param $query
     * @param array $array
     * @return Router
     */
    public static function get($query, $array = []){
        $queryArray = [];

        foreach ($array as $k => $arr){
            $queryArray[$k] = $arr;
        }

        $queryArray += ['url' => $query];

        $queryArray += ['method' => 'GET'];

        array_push(self::$routes, $queryArray);

        self::$queryArray = $queryArray;

        return new Router();
    }

    /**
     * @param $query
     * @param array $array
     * @return Router
     */
    public static function post($query, $array = []){

        $queryArray = [];

        foreach ($array as $k => $arr){
            $queryArray[$k] = $arr;
        }

        $queryArray += ['url' => $query];

        $queryArray += ['method' => 'POST'];
        array_push(self::$routes, $queryArray);

        self::$queryArray = $queryArray;

        return new Router();
    }

    /**
     * @param $regParams
     */
    public static function with($regParams){

            self::mathWithRegParam($regParams);
    }

    public static function group(){


    }

    /**
     * @param $regString
     */
    public static function mathWithRegParam($regString){
        if(preg_match_all('~{[\w]+}~', self::$queryArray['url'], $params)){

            foreach ($params as $arrParam){

                foreach ($arrParam as $param){
                    $arrKey = substr($param, 1, -1);

                    if(array_key_exists($arrKey, $regString)){
                        self::$queryArray['url'] = str_replace($param, $regString[$arrKey], self::$queryArray['url']);
                    }

                }

                self::changeUrl();

            }
        }
    }

    /**
     *
     */
    public static function changeUrl(){

        foreach (self::$routes as $k => $route){
            if($route['uses'] === self::$queryArray['uses']){
                self::$routes[$k]['url'] = self::$queryArray['url'];
            }

        }

    }


}
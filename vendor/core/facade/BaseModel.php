<?php

namespace vendor\core\facade;

require(ROOT . 'config/params.php');

abstract class BaseModel extends \PDO{
    /**
     * @var string
     */
    protected static $table = '' ;
    /**
     * @var string
     */
    protected static $selectQuery = "";
    /**
     * @var string
     */
    protected static $deleteQuery = "";

    /**
     * @var string
     */
    protected static $updateQuery = "";

    /**
     * @var string
     */
    protected static $updateQueryParams = "";
    /**
     * @var string
     */
    protected static $selectQueryParams = "*";
    /**
     * @var string
     */
    protected static $where = '';
    /**
     * @var string
     */
    protected static $indexes = '';
    /**
     * @var string
     */
    protected static $values = '';

    /**
     * @return BaseModel
     */
    protected static function connect(){
        try {
            return new static('mysql:host=localhost;dbname=' . SCHEMA, USER, PASSWORD);
        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /**
     * @param array $query
     * @return BaseModel
     */
    public static function select($query = []){
        static::$selectQueryParams = $query ? $query :  static::$selectQueryParams;
        return static::connect();
    }

    /**
     * @param array $params
     * @return bool
     */
    public static function create($params = []){
        foreach ($params as $k => $param){
            static::$indexes.= "$k, ";
            static::$values .= ":$k, ";
        }
        $query = static::connect()->prepare('INSERT INTO ' . static::$table . ' ('.substr(static::$indexes, 0, -2) . ')' . ' VALUES ' . '('  .  substr(static::$values, 0, -2) . ')');

        foreach ($params as $k => $param){
            $query->bindParam(":$k", $$k);
            $$k = $param;
        }

        if($query->execute()){
            return true;
        }
        return false;

    }


    /**
     * @return bool
     */
    public static function delete(){
        $table = self::$table ? self::$table : static::$table;
        static::$deleteQuery = 'DELETE FROM ' . $table . static::$where;
        static::$where = '';
        if(static::connect()->query(static::$deleteQuery)){
            return true;
        }
        return false;
    }

    /**
     * @param array $params
     * @return bool
     */

    public static function update($params = []){
        foreach ($params as $k => $param){
            static::$updateQueryParams .=  "$k='$param',";
        }
        $table = self::$table ? self::$table : static::$table;
        static::$updateQuery = 'UPDATE ' . $table . ' SET ' . substr(static::$updateQueryParams, 0, -1) . static::$where;
        if(static::connect()->query(static::$updateQuery)){
            return true;
        }
        return false;
    }

    /**
     * @param $column
     * @param $action
     * @param $value
     * @return BaseModel
     */
    public static function where($column = null, $action = null , $value = null){
        static::$where .= static::$where ? " AND $column $action $value" : " WHERE $column $action $value";
        return static::connect();
    }


    /**
     * @return BaseModel
     */
    public static function get(){
        $table = self::$table ? self::$table : static::$table;
        static::$selectQuery = 'SELECT ' . static::$selectQueryParams . ' FROM ' . $table . static::$where;
        static::$where = '';
        return static::connect();
    }


    /**
     * @return \PDOStatement
     */
    public static function first(){
        $query = static::$selectQuery;
        try{
            return static::get()->query("$query") ?  static::get()->query("$query")->fetchObject() : false;
        }catch (\PDOException $e){
            $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public static function all(){
        $query = static::$selectQuery;
        try{
            return static::get()->query("$query") ?  static::get()->query("$query")->fetchAll(\PDO::FETCH_CLASS) : false;
        }catch (\PDOException $e){
            $e->getMessage();
        }
    }

}
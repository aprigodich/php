<?php

namespace vendor\core;

require (ROOT . 'config/params.php');

Class DB {

    public static function connect($query, $result = false, $allQuery = false){
        $link = mysqli_connect(HOST, USER, PASSWORD, SCHEMA);
        if($result){
            if($allQuery){
                $query =  mysqli_fetch_all(mysqli_query($link, $query), MYSQLI_ASSOC);
            }else{
                $query =  mysqli_fetch_assoc(mysqli_query($link, $query));
            }
            mysqli_close($link);
            return $query;
        }else{
            $query = mysqli_query($link, $query);
            mysqli_close($link);
            return $query;
        }
    }
}
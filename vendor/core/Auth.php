<?php

use vendor\core\DB;
use app\User;

class Auth{

    public static $redirect = '/';

    /**
     * @return array|bool|mysqli_result|null|string
     */
    public static function login($username, $password){

        if(isset($username) && isset($password)) {
                $query = DB::connect("SELECT * FROM `users` WHERE `username` = '$username'", true);
                if($query){
                    self::sessionStart($query);
                    redirect('/');
                }else{
                    echo 'Your login or name not is found';
                }
        }else{
            echo 'Fields is empty';
        }

    }

    /**
     * @param $username
     * @param $password
     * @param $confirm_password
     * @return string
     * @throws ErrorException
     */
    public static function register($username, $password, $confirm_password){

        if(isset($username) && isset($password) && isset($confirm_password)){
            if($password === $confirm_password){
                if( !DB::connect("SELECT * FROM `users` WHERE `username` = '$username'", true)){
                    try{
                        DB::connect("INSERT INTO `users` (`username`, `password`) VALUES ('$username', '$password')");
                        $query = DB::connect("SELECT * FROM `users` WHERE `username` = '$username'", true);
                        self::sessionStart($query);
                        redirect(self::$redirect);
                    }
                    catch (ErrorException $e){
                        throw new \ErrorException('User not saved');
                    }
                }else{
                    throw new \ErrorException('User exist, change username');
                }
            }else{
                return 'Your password is not same';
            }
        }else{
            return 'Check your login or password';
        }

    }

    /**
     *
     */
    public static function logout(){
        self::session();
        unset($_SESSION['user_id']);
        return redirect('/');

    }

    /**
     * @return bool|PDOStatement
     */
    public static function user(){
        self::session();
        if(isset($_SESSION['user_id']) && $id = $_SESSION['user_id']){
            $query = User::where('id', '=', $id)->get()->first();
//            $query = DB::connect("SELECT * FROM `users` WHERE `id` = '$id'", true);
            return $query;
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function check(){
        self::session();

            if(isset($_SESSION['user_id'])){
                return true;
            }
        return false;
    }

    /**
     * @param $query
     */
    public static function sessionStart($query){
        session_start();
        $_SESSION['user_id'] = $query['id'];
    }

    /**
     *
     */
    private static function session(){
        if(!isset($_SESSION)){
            session_start();
        }
    }

}
<?php

namespace vendor\core;

class View {

    public static function make($pathToView, $items = []){

        $str =  self::buffer($pathToView, $items);

        if(preg_match('~@require\(\'([\w\/]+)\'\)~', $str, $paramsRequire)){
                $layoutName = $paramsRequire[1];
                $layoutMain = self::buffer($layoutName, $items);

                preg_match_all('~@field\(\'([\w]+)\'\)~', $layoutMain, $paramsLayout);

                preg_match_all('~@section\(\'([\w]*)\'\)([\w\W]*)@end~', $str, $paramsSection);

                array_shift($paramsSection);

                foreach ($paramsLayout[1] as $paramLayout){
                    foreach ($paramsSection[0] as $k => $paramSection){
                        if($paramLayout === $paramSection){
                            $layoutMain = str_replace("@field('$paramLayout')", $paramsSection[1][$k], $layoutMain);
                        }
                    }
                }
                echo $layoutMain;
                return new View();
        }else{
            echo preg_replace('~@field\([\w]+\)~', '', $str);
            return new View();
        }
    }

//    public static function with($array){
//        extract($array, EXTR_SKIP);
//    }

    private static function buffer($pathToView, $items = []){
        if($items){
            foreach ($items as $k => $item) {
                $$k = $item;
            }
        }

        ob_start();
        require_once (ROOT . 'resources/view/' . $pathToView . '.vyw.php');
        return ob_get_clean();
    }

}
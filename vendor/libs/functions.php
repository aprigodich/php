<?php

function redirect($url = ''){
    header("Location: $url");
}

function abort ($number, $text){
    throw new ErrorException('Error:' . $number . ' - ' . $text);
}
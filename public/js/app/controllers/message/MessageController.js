
window.onload = function () {
  // MessageController.getMessage();
  setInterval('MessageController.getMessage()', 1000);
};


MessageController = {
    user: document.getElementById('user'),
    select:  document.getElementById('select'),
    messages:  document.getElementById('messages'),
    message:  document.getElementById('message'),
    audio: document.getElementById('audio'),

    removeChild: function () {
        while (this.select.childNodes.length){
            this.select.removeChild(this.select.lastChild);
        }
    },

    hideMultiple: function () {
        this.select.hidden = true;
        this.removeChild();
    },


    getUserValue: function(event) {
        this.user.value = event.target.text;
    },

    sendMessage: function() {

        this.removeChild();

        var data = {'user' : this.user.value, 'message' : this.message.value};

        Ajax.post('/message/create', data);
        this.message.value = '';
    },

    getMessage: function() {
        var h2 = document.createElement('h2');
        var p = document.createElement('p');
        var div = document.createElement('div');
        var req = Ajax.get('/message');
        req.onreadystatechange = function() {
            if (req.readyState == 4 && req.status == 200 && req.response.length > 0) {
                var response = JSON.parse(req.response);
                div.setAttribute('class' , 'col-lg-12');
                h2.textContent = 'Message from: ' + response.from_who_sended;
                p.textContent = 'Created : ' + response.created_at;
                var cloneP = p.cloneNode(false);
                cloneP.textContent = 'Message: ' + response.message;
                div.appendChild(h2);
                div.appendChild(p);
                div.appendChild(cloneP);
                this.messages.appendChild(div);
                this.audio.play();
            }
        }.bind(this);
    }

};




DiaryController = {

    diaryUsername: document.getElementById('diaryUsername'),
    diaryEmail: document.getElementById('diaryEmail'),
    diaryText: document.getElementById('diaryText'),
    diaryImage: document.getElementById('diaryImage'),
    diary: document.getElementById('diary'),

    diaryCratePreview: function () {
        var div = document.createElement('dir');
        div.setAttribute('className', 'col-md-6');
        var h3 = document.createElement('h3');
        var image = new Image();
        var req = Ajax.postFile('/diary/preview','diaryImage', this.diaryImage.files[0]);
        req.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200 && this.response.length > 0) {
                image.src = this.response;
                h3.textContent = 'Пользователь ' + DiaryController.diaryUsername.value;
                var h3Email = h3.cloneNode(false);
                h3Email.textContent = 'Мэил ' + DiaryController.diaryEmail.value;
                var h3Text = h3.cloneNode(false);
                h3Text.textContent = 'Задача ' + DiaryController.diaryText.value;
                div.appendChild(h3Text);
                div.appendChild(h3);
                div.appendChild(h3Email);
                div.appendChild(image);
                DiaryController.diary.appendChild(div);
            }
        };
    }

};

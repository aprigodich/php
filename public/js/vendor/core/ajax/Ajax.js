
Ajax = {
    req: new XMLHttpRequest(),

    get: function (url) {
        this.req.open('GET', url, true);
        this.req.setRequestHeader('Content-Type', 'text/plain');
        this.req.send();
        return this.req;
    },

    post:  function (url, data) {
        this.req.open('POST', url, true);
        this.req.setRequestHeader('Content-Type', 'text/plain');
        this.req.send(JSON.stringify(data));
        return this.req;
    },

    postFile:  function (url, filename, data) {
        this.req.open('POST', url, true);
        var formdata = new FormData();
        formdata.append(filename, data);
        this.req.send(formdata);
        return this.req;
    }
};


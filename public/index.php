<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

function exception_handler($exception) {
    echo "Неперехваченное исключение: " , $exception->getMessage(), "\n";
}

set_exception_handler('exception_handler');
define('ROOT', dirname(__DIR__) . '/');
define('APP', dirname(__DIR__) . '/app/');
define('CORE', dirname(__DIR__) . '/vendor/core/');

require_once (ROOT . 'vendor/core/Router.php');
require_once (ROOT . 'vendor/core/Auth.php');
require_once (ROOT . 'routes.php');
require_once (ROOT . 'vendor/libs/functions.php');

spl_autoload_register(function ($class){
   $file = str_replace('\\', '/', ROOT . $class ) . '.php';
    if(is_file($file)){
        require_once ($file);
    }
});

if(!Router::run($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD'])){
    throw new ErrorException('Route Not Found');
}



